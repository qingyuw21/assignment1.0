# Uno Game
A socket based uno game writing in java.

# Setup
Install java
```
sudo apt install default-jdk
```

# How to play
Start a server by
```
java startGame
```
and follow the prompt

Then start as many client as you want as players in Client folder

```
java Client
```
Follow the prompt to enter the port number of the server to join the same room as other players. Then enjoy playing uno!