import Game.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class startGame {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner sc= new Scanner(System.in);
        System.out.println("Welcome to UNO! How many player will play?");
        int numOfPlayer = sc.nextInt();
        System.out.println("Port number?");
        int port = sc.nextInt();
        Game game = new Game(numOfPlayer, port, 0);
        game.startGame();
    }
}
