package Test;

import Player.*;
import Card.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class PlayerTest {
    Player player;
    @Test
    public void simpleTest() {
        player = new Player();
        Card testCard = new NumberCard(6, 2);
        assertEquals(0, player.getNumOfCards());
        player.drawOneCard(testCard);
        assertEquals(1, player.getNumOfCards());
        player.playCard(testCard);
        assertEquals(0, player.getNumOfCards());
    }

    @Test
    public void drawMultipleCardTest() {
        player = new Player();
        Card[] cards = new Card[8];
        for (int i = 0; i < 8; ++i) {
            cards[i] = new NumberCard(i, i % 4);
        }
        player.drawMultipleCards(cards);
        assertEquals(player.getNumOfCards(), 8);
        NumberCard first = new NumberCard(0, 0);
        NumberCard second = new NumberCard(1, 1);
        NumberCard third = new NumberCard(2, 2);
        NumberCard fourth = new NumberCard(3, 3);
        assertTrue(player.returnIfContainsLegal(first).size() > 0);
        assertTrue(player.returnIfContainsLegal(second).size() > 0);
        assertTrue(player.returnIfContainsLegal(third).size() > 0);
        assertTrue(player.returnIfContainsLegal(fourth).size() > 0);
        player.playCard(cards[0]);
        player.playCard(cards[4]);
        assertTrue(player.returnIfContainsLegal(new ActionCard(0, 0)).size() == 0);
        assertTrue(player.returnIfContainsLegal(new NumberCard(1, 0)).size() > 0);
    }

    @Test
    public void wildCardInHandTest() {
        player = new Player();
        player.drawOneCard(new ActionCard(4, 4));
        assertTrue(player.returnIfContainsLegal(new NumberCard(0, 0)).size() > 0);
    }

    @Test
    public void getHandTest() {
        player = new Player();
        assertEquals(5, player.getHand().size());
    }
}
