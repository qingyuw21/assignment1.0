package Test;

import Card.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class ActionCardTest {
    ActionCard TestCard;
    @Test
    public void comprehensiveTest() {
        TestCard = new ActionCard(0, 0);
        assertEquals(TestCard.getAction(), 0);
        assertEquals(TestCard.getColor(), 0);
        assertTrue(TestCard.getType());
        TestCard.setColor(1);
        TestCard.setAction(2);
        assertEquals(TestCard.getAction(), 2);
        assertEquals(TestCard.getColor(), 1);
    }
}
