package Test;

import Discard.*;
import Card.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DiscardTest {
    Discard discard;
    @Test
    public void simpleTest() {
        discard = new Discard();
        NumberCard TestCard = new NumberCard(9, 2);
        discard.discardCard(TestCard);
        assertEquals(discard.getNumOfCards(), 1);
        NumberCard topCard = (NumberCard)discard.getTop();
        assertEquals(topCard.getColor(), 2);
        assertEquals(topCard.getNumber(), 9);
        assertFalse(topCard.getType());
    }
    @Test
    public void multipleDiscardAndReshuffleTest() {
        discard = new Discard();
        Card TestCard = new NumberCard(0,0);
        discard.discardCard(TestCard);
        TestCard = new NumberCard(1, 1);
        discard.discardCard(TestCard);
        TestCard = new NumberCard(2, 2);
        discard.discardCard(TestCard);
        TestCard = new NumberCard(3, 3);
        discard.discardCard(TestCard);
        assertEquals(discard.getNumOfCards(), 4);
        NumberCard topCard = (NumberCard)discard.getTop();
        assertEquals(topCard.getColor(), 3);
        assertEquals(topCard.getNumber(), 3);
        assertFalse(topCard.getType());
        ArrayList<Card> reshuffle = discard.reshuffle();
        int size = reshuffle.size();
        assertEquals(size, 3);
        for (int i = 0; i < size; ++i) {
            NumberCard tempCard = (NumberCard)reshuffle.get(i);
            assertEquals(tempCard.getColor(), i);
            assertEquals(tempCard.getNumber(), i);
        }
    }
    @Test
    public void multipleTypeCardDiscardTest() {
        discard = new Discard();
        Card TestCard = new NumberCard(0, 0);
        discard.discardCard(TestCard);
        TestCard = new ActionCard(1, 1);
        discard.discardCard(TestCard);
        TestCard = new NumberCard(2, 2);
        discard.discardCard(TestCard);
        TestCard = new ActionCard(3, 3);
        discard.discardCard(TestCard);
        assertEquals(discard.getNumOfCards(), 4);
        ActionCard topCard = (ActionCard)discard.getTop();
        assertEquals(topCard.getColor(), 3);
        assertEquals(topCard.getAction(), 3);
        assertTrue(topCard.getType());
        ArrayList<Card> reshuffle = discard.reshuffle();
        int size = reshuffle.size();
        assertEquals(size, 3);
        boolean type = false;
        for (int i = 0; i < size; ++i) {
            Card tempCard = reshuffle.get(i);
            assertEquals(tempCard.getColor(), i);
            assertEquals(tempCard.getType(), type);
            type = !type;
        }
    }
}
