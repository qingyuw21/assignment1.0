package Test;

import Card.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class NumberCardTest {
    NumberCard TestCard;
    @Test
    public void comprehensiveTest() {
        TestCard = new NumberCard(5, 0);
        assertEquals(TestCard.getNumber(), 5);
        assertEquals(TestCard.getColor(), 0);
        assertFalse(TestCard.getType());
        TestCard.setColor(1);
        TestCard.setNumber(9);
        assertEquals(TestCard.getColor(), 1);
        assertEquals(TestCard.getNumber(), 9);
    }
}
