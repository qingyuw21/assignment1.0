package Test;

import Discard.Discard;
import Game.*;
import Deck.*;
import Player.*;
import Card.*;
import org.junit.Test;

import javax.swing.*;

import java.io.IOException;

import static org.junit.Assert.*;

public class GameTest {
    Game game;

    @Test
    public void initTest() {
        game = new Game(2);
        Deck deck = game.getDeck();
        Discard discard = game.getDiscard();
        Player[] players = game.getPlayers();
        assertEquals(7, players[0].getNumOfCards());
        assertEquals(7, players[1].getNumOfCards());
        assertEquals(94, deck.getNumOfCards() + discard.getNumOfCards());
        assertFalse(discard.getTop().getType());
    }

    @Test
    public void initTestV2() {
        game = new Game(2, 12345, 2);
        Deck deck = game.getDeck();
        Discard discard = game.getDiscard();
        Player[] players = game.getPlayers();
        assertEquals(7, players[0].getNumOfCards());
        assertEquals(7, players[1].getNumOfCards());
        assertEquals(94, deck.getNumOfCards() + discard.getNumOfCards());
        assertFalse(discard.getTop().getType());
    }

    @Test
    public void skipAndReverseTest() {
        game = new Game(10);
        for (int i = 0; i < 10; ++i) {
            assertEquals(i, game.getTurn());
            game.handleSkip();
        }
        assertEquals(0, game.getTurn());
        game.handleReverse();
        for (int i = 9; i >=0; --i) {
            game.handleSkip();
            assertEquals(i, game.getTurn());
        }
    }

    @Test
    public void drawTwoTest() {
        game = new Game(3);
        Player[] players = game.getPlayers();
        game.handleDrawTwo();
        assertEquals(9, players[1].getNumOfCards());
        game.handleSkip();
        game.handleDrawTwo();
        assertEquals(9, players[2].getNumOfCards());
        game.handleSkip();
        game.handleDrawTwo();
        assertEquals(9, players[0].getNumOfCards());
    }

    @Test
    public void wildAndWildDrawFourTest() {
        game = new Game(6);
        Player[] players = game.getPlayers();
        game.handleWildDrawFour();
        assertEquals(11, players[1].getNumOfCards());
    }

    @Test
    public void switchTest() {
        game = new Game(2);
        int turn = game.getTurn();
        game.handleDoubleSkip();
        game.handleSkip();
        int next = game.getTurn();
        assertEquals(turn, next);

        game = new Game(3);
        turn = game.getTurn();
        game.handleDoubleSkip();
        game.handleSkip();
        next = game.getTurn();
        assertEquals(turn, next);

        game = new Game(5);
        game.handleDoubleSkip();
        game.handleSkip();
        next = game.getTurn();
        assertEquals(3, next);
    }

    @Test
    public void oneMoreRoundTest() {
        game = new Game(5);
        int turn = game.getTurn();
        game.handleOneMoreRound();
        game.handleSkip();
        int oneMore = game.getTurn();
        assertEquals(turn, oneMore);
    }

    @Test
    public void handlePlayerTest() {
        game = new Game(2, 12345);
        // Player draw one card if no legal card
        assertNull(game.handlePlayer(game.getPlayers()[0], new NumberCard(9, 2),0));
        assertEquals(8, game.getPlayers()[0].getNumOfCards());

        // Player play one number card if color is match
        Card firstCard = game.handlePlayer(game.getPlayers()[0], new NumberCard(3, 1),1);
        assertEquals(1, firstCard.getColor());
        assertFalse(firstCard.getType());
        assertEquals(5, ((NumberCard)firstCard).getNumber());
        assertEquals(7, game.getPlayers()[0].getNumOfCards());

        // Player play one action card if action is match
        Card secondCard = game.handlePlayer(game.getPlayers()[0], new ActionCard(1,1), 0);
        assertEquals(1, secondCard.getColor());
        assertTrue(secondCard.getType());
        assertEquals(1, ((ActionCard)secondCard).getAction());

        // Player draw wild card and play it after no color is match
        game.getPlayers()[0].drawOneCard(new ActionCard(4, 4));
        assertEquals(7, game.getPlayers()[0].getNumOfCards());
        Card wild = game.handlePlayer(game.getPlayers()[0], new NumberCard(0, 1), 0);
        assertEquals(4, wild.getColor());
        assertTrue(wild.getType());
        assertEquals(4, ((ActionCard) wild).getAction());
        assertEquals(6, game.getPlayers()[0].getNumOfCards());

        // Player draw one card and play it out immediately
        Card draw = game.handlePlayer(game.getPlayers()[0], new NumberCard(0, 2), 0);
        assertNull(draw);
        draw = game.handlePlayer(game.getPlayers()[0], new NumberCard(0, 2), 0);
        assertNotNull(draw);
        assertFalse(draw.getType());
        assertEquals(2, draw.getColor());
        assertEquals(2, ((NumberCard)draw).getNumber());
        game.handleWild(1);
    }

    @Test
    public void testprintCurrentGameState() {
        game = new Game(2, 12345);
        String actual = game.printCurrentGameState();
        assertEquals("You are Player No.0 and the order is clockwise.\tThere are 93 cards in deck now.\tTop card on the discard pile is (yellow 5).\tYou have 7 cards in your hands: [red 8] [yellow Reverse] [yellow 5] [blue 7] [blue 6] [blue 6] [blue 1] .\tAnd you could play <yellow Reverse> <yellow 5> .\tOr do you want to draw a card?",
                actual);
    }
}
