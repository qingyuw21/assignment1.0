package Test;

import Deck.*;
import Card.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DeckTest {
    Deck deck;

    @Test
    public void simpleTest() {
        deck = new Deck();
        assertEquals(deck.getNumOfCards(), 108);
        deck.drawTop();
        assertEquals(deck.getNumOfCards(), 107);
    }

    @Test
    public void drawOneTest() {
        deck = new Deck(12345);
        assertEquals(deck.getNumOfCards(), 108);
        NumberCard topCard = (NumberCard) deck.drawTop();
        assertFalse(topCard.getType());
        assertEquals(topCard.getColor(), 3);
        assertEquals(topCard.getNumber(), 7);
        assertEquals(deck.getNumOfCards(), 107);
    }

    @Test
    public void drawMultipleTopTest() {
        deck = new Deck(12345);
        assertEquals(deck.getNumOfCards(), 108);
        Card[] topCards = new Card[7];
        topCards = deck.drawMultipleTop(7);

        assertFalse(topCards[0].getType());
        assertEquals(topCards[0].getColor(), 3);
        assertEquals(((NumberCard)topCards[0]).getNumber(), 7);

        assertFalse(topCards[1].getType());
        assertEquals(topCards[1].getColor(), 3);
        assertEquals(((NumberCard)topCards[1]).getNumber(), 6);

        assertFalse(topCards[2].getType());
        assertEquals(topCards[2].getColor(), 3);
        assertEquals(((NumberCard)topCards[2]).getNumber(), 6);

        assertTrue(topCards[3].getType());
        assertEquals(topCards[3].getColor(), 1);
        assertEquals(((ActionCard)topCards[3]).getAction(), 1);

        assertFalse(topCards[4].getType());
        assertEquals(topCards[4].getColor(), 1);
        assertEquals(((NumberCard)topCards[4]).getNumber(), 5);

        assertFalse(topCards[5].getType());
        assertEquals(topCards[5].getColor(), 0);
        assertEquals(((NumberCard)topCards[5]).getNumber(), 8);

        assertFalse(topCards[6].getType());
        assertEquals(topCards[6].getColor(), 3);
        assertEquals(((NumberCard)topCards[6]).getNumber(), 1);

        assertEquals(deck.getNumOfCards(), 101);
    }

    @Test public void setNewDeckTest() {
        deck = new Deck(12345);
        assertEquals(deck.getNumOfCards(), 108);
        deck.drawMultipleTop(108);
        assertEquals(deck.getNumOfCards(), 0);
        ArrayList<Card> newDeck = new ArrayList<Card>();
        newDeck.add(new NumberCard(9, 9));
        newDeck.add(new ActionCard(0, 0));
        deck.setUpNewDeck(newDeck);
        assertEquals(deck.getNumOfCards(), 10);
        deck.drawMultipleTop(2);
        newDeck.clear();
        newDeck.add(new ActionCard(4,4));
        assertEquals(newDeck.size(), 1);
        deck.setUpNewDeck(newDeck);
        assertEquals(deck.getNumOfCards(), 9);
        ActionCard topCard = (ActionCard) deck.drawTop();
        assertEquals(topCard.getAction(), 1);
        assertEquals(topCard.getColor(), 3);
        assertTrue(topCard.getType());
    }
}
