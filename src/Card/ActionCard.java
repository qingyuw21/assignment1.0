package Card;

import Card.Card;

import java.util.HashMap;

public class ActionCard extends Card {
    // 0:skip 1:reverse 2:draw two cards 3:wild 4:wild draw four 5:double skip 6:one more round
    private int action;

    private static HashMap<Integer, String> actionSet;
    static {
        actionSet = new HashMap<Integer, String>();
        actionSet.put(0, "Skip");
        actionSet.put(1, "Reverse");
        actionSet.put(2, "Draw Two");
        actionSet.put(3, "");
        actionSet.put(4, "Draw four");
        actionSet.put(5, "Double Skip");
        actionSet.put(6, "One More Round");
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof ActionCard){
            ActionCard c = (ActionCard) o;
            // Check if two cards are matched according to UNO rules
            return (this.action == c.action ||
                    this.getColor() == c.getColor()) &&
                    this.getType() == c.getType();
        } else
            return false;
    }

    @Override
    public String toString() {
        return String.format("%s %s", coloSet.get(this.getColor()), actionSet.get(action));
    }

    /**
     * Constructor for ActionCard object.
     * @param action action of the card
     * @param color color of the card
     */
    public ActionCard(int action, int color) {
        setType(true);
        setAction(action);
        setColor(color);
    }

    /**
     * Setter for action variable.
     * @param action action of the card
     */
    public void setAction(int action) {
        this.action = action;
    }

    /**
     * Getter for action variable.
     * @return action of the card
     */
    public int getAction() {
        return action;
    }
}
