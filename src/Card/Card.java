package Card;

import java.util.HashMap;

public abstract class Card {
    // represents type of card true:action false:number
    private boolean type;
    // represents colors 0:red 1:yellow 2:green 3:blue 4:wild
    private int color;

    public static HashMap<Integer, String> coloSet;
    static {
        coloSet = new HashMap<Integer, String>();
        coloSet.put(0, "red");
        coloSet.put(1, "yellow");
        coloSet.put(2, "green");
        coloSet.put(3, "blue");
        coloSet.put(4, "wild");
    }

    /**
     * Setter for type variable.
     * @param type type of card
     */
    public void setType(boolean type){
        this.type = type;
    }

    /**
     * Setter for color variable.
     * @param color color of card
     */
    public void setColor(int color) {
        this.color = color;
    }

    /**
     * Getter for type variable.
     * @return type of the card
     */
    public boolean getType() {
        return type;
    }

    /**
     * Getter for color variable.
     * @return color of the card
     */
    public int getColor() {
        return color;
    }
}
