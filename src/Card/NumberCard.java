package Card;

import Card.Card;

public class NumberCard extends Card {
    private int number;

    @Override
    public boolean equals(Object o){
        if(o instanceof NumberCard){
            NumberCard c = (NumberCard)o;
            return (this.number == c.number ||
                    this.getColor() == c.getColor()) &&
                    this.getType() == c.getType();
        } else
            return false;
    }

    @Override
    public String toString() {
        if (number == -1) {
            return coloSet.get(this.getColor());
        }
        return String.format("%s %s", coloSet.get(this.getColor()), Integer.toString(number));
    }

    /**
     * Constructor for NumberCard object.
     * @param number action of the card
     * @param color color of the card
     */
    public NumberCard(int number, int color) {
        setType(false);
        setNumber(number);
        setColor(color);
    }

    /**
     * Setter for number variable.
     * @param number action of the card
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Getter for number variable.
     * @return number of the card
     */
    public int getNumber() {
        return number;
    }
}
