
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * This class implements java socket client
 * @author pankaj
 *
 */
public class Client {

    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException, InterruptedException {
        //get the localhost IP address, if server is running on some other IP, you need to use that
        InetAddress host = InetAddress.getLocalHost();
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input the port number for room");
        Socket socket = new Socket(host.getHostName(), sc.nextInt());

        while (true) {
            InputStream is = new DataInputStream(socket.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String recv = reader.readLine();
            if (recv != null && !recv.equals("Invalid response.")) {
                String msg = recv.replace('\t', '\n');
                System.out.println(msg);
            }
            while (true && !recv.contains("don't") && !recv.contains("!")) {
                String response = sc.nextLine();
                if (response != null && !response.contains("\n")) {
                    OutputStream output = socket.getOutputStream();
                    PrintWriter writer = new PrintWriter(output, true);
                    writer.println(response);
                    System.out.println("================================================");
                    break;
                }
            }
            if (recv.contains("win")) {
                break;
            }
        }
    }
}