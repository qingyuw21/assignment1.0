package Discard;

import Card.*;

import java.util.ArrayList;

public class Discard {
    private int numOfCards;
    private Card top;
    private ArrayList<Card> discard;

    /**
     * Constructor of the Discard class.
     */
    public Discard() {
        numOfCards = 0;
        discard = new ArrayList<Card>();
    }

    /**
     * Discard one card to the discard pile
     * @param card card to be discard
     */
    public void discardCard(Card card) {
        if (numOfCards == 0) {
            top = card;
        } else {
            discard.add(top);
            top = card;
        }
        numOfCards++;
    }

    /**
     * Get the top card on the discard pile
     * @return card on the top of discard pile
     */
    public Card getTop() {
        return top;
    }

    /**
     * Reshuffle(not really) the whole discard to deck.
     * @return all the cards in the discard
     */
    public ArrayList<Card> reshuffle() {
        numOfCards = 1;
        return discard;
    }

    /**
     * Getter for number of cards in discard pile.
     * @return number of cards in discard pile
     */
    public int getNumOfCards() {return numOfCards;}
}
