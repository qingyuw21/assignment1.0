package Player;

import Card.*;

import javax.print.attribute.standard.NumberUp;
import java.util.ArrayList;
import java.util.HashMap;

public class Player {
    private int numOfCards;
    private HashMap<Integer, ArrayList<Card>> hand;

    /**
     * Constructor for Player object.
     */
    public Player() {
        numOfCards = 0;
        hand = new HashMap<Integer, ArrayList<Card>>();
        for (int i = 0; i < 5; ++i) {
            hand.put(i, new ArrayList<Card>());
        }
    }

    public HashMap<Integer, ArrayList<Card>> getHand() {
        return hand;
    }

    /**
     * Getter for number of cards in player.
     * @return number of cards a player has
     */
    public int getNumOfCards() {
        return numOfCards;
    }

    /**
     * Player draw multiple cards
     * @param cards cards to be drawn
     */
    public void drawMultipleCards(Card[] cards) {
        for(Card i : cards) {
            add(i);
        }
    }

    /**
     * Helper function to add card into player's hand.
     * @param card card to be added
     */
    private void add(Card card) {
        numOfCards++;
        ArrayList<Card> temp = hand.get(card.getColor());
        temp.add(card);
    }

    /**
     * Player draw one card.
     * @param card card to be drawn
     */
    public void drawOneCard(Card card) {
        add(card);
    }

    /**
     * Player play(remove) one card from hand.
     * @param card card to be played out
     */
    public void playCard(Card card) {
        ArrayList<Card> colorSet = hand.get(card.getColor());
        numOfCards--;

        colorSet.remove(card);
    }

    /**
     * Return empty Arraylist if player doesn't have any card to play, otherwise return Arraylist of cards
     * that could be played out.
     * @param card card on top of discard pile
     * @return Arraylist that contains cards that could be played
     */
    public ArrayList<Card> returnIfContainsLegal(Card card) {
        ArrayList<Card> ret = new ArrayList<Card>();
        int color = card.getColor();
        if (hand.get(color).size() > 0) {
            ret.addAll(hand.get(color));
        }
        ret.addAll(returnIfNumberMatch(card));
        return ret;
    }

    /**
     * Helper function to return a list of card that number or action is matched.
     * @param card card to be compared with
     * @return Arraylist of cards that matched with card by number or action.
     */
    private ArrayList<Card> returnIfNumberMatch(Card card) {
        ArrayList<Card> ret = new ArrayList<Card>();
        int color = card.getColor();
        int number = card instanceof NumberCard ? ((NumberCard) card).getNumber() : ((ActionCard)card).getAction();
        for (int i = 0; i < 4; ++i) {
            if (i == color) {
                continue;
            }
            ArrayList<Card> colorSet = hand.get(i);
            for (Card cur : colorSet) {
                int curNumber = cur instanceof NumberCard ? ((NumberCard) cur).getNumber() : ((ActionCard) cur).getAction();
                if (cur.getType() == card.getType() && curNumber == number) {
                    ret.add(cur);
                }
            }
        }
        if (ret.size() == 0 && hand.get(4).size() > 0) {
            ret.addAll(hand.get(4));
        }
        return ret;
    }
}
