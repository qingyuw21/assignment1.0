package Game;

import Card.*;
import Deck.Deck;
import Discard.Discard;
import Player.Player;

import javax.swing.*;
import java.io.*;
import java.lang.ClassNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Game {
    private static ServerSocket server;
    private ArrayList<Socket> clients;
    private int port;

    private Player players[];
    private Deck deck;
    private Discard discard;
    private NumberCard wild;
    private int turn;
    private int order;
    private int numOfPlayers;
    private int numOfAI;

    /**
     * Constructor of the Game.Game object
     * @param numOfPlayers number of players participated in the game
     */
    public Game(int numOfPlayers) {
        this.numOfPlayers = numOfPlayers;
        clients = new ArrayList<Socket>();
        init();
        dealInitialCardsToPlayers();
        assignCardToDiscard();
    }

    /**
     *
     * @param numOfPlayers
     * @param port
     * @param numOfAI
     */
    public Game(int numOfPlayers, int port, int numOfAI) {
        this.numOfPlayers = numOfPlayers;
        this.port = port;
        this.numOfAI = numOfAI;
        clients = new ArrayList<Socket>();
        init();
        dealInitialCardsToPlayers();
        assignCardToDiscard();
    }

    /**
     * Test Constructor for game
     * @param numOfPlayers number of players
     * @param seed seed for random deck
     */
    public Game(int numOfPlayers, int seed) {
        this.numOfPlayers = numOfPlayers;
        init();
        deck = new Deck(seed);
        dealInitialCardsToPlayers();
        assignCardToDiscard();
    }

    /**
     * game entry
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void startGame() throws IOException {
        server = new ServerSocket(port);
        while (true) {
            Socket s = server.accept();
            System.out.println( "Connection from " + s);
            clients.add(s);
            if (clients.size() == numOfPlayers) {
                break;
            }
        }
        System.out.println("All players have joined the game. Game starting...");
        gameLoop();
    }

    /**
     * Game loop
     * @throws IOException
     */
    public void gameLoop() throws IOException {
        while (true) {
            int curTurn = getTurn();

            String gameState = printCurrentGameState();
            Socket curPlayer = clients.get(curTurn);
            sendToPlayer(clients.get(curTurn), gameState);

            if (!gameState.contains("don't")) {
                InputStream input = curPlayer.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                boolean flag = false;

                // wait for player's response
                while (true) {
                    String response = reader.readLine();
                    // player respond
                    if (response != null && response != "") {
                        int idx;
                        try {
                            // Draw a card if player want to
                            if (response.contains("Draw")) {
                                dealCardToPlayer(players[turn]);
                                break;
                            } else if (flag) {
                                handleWild(Integer.parseInt(response));
                                flag = false;
                                checkDeck(4);
                                break;
                            } else {
                                idx = Integer.parseInt(response);
                            }
                        } catch (NumberFormatException exception) {
                            sendToPlayer(clients.get(curTurn), "Invalid response.");
                            response = null;
                            continue;
                        }
                        // Handle player's played card
                        if (handleTurn(idx)) {
                            // Handle wild color selection
                            sendToPlayer(clients.get(curTurn),
                                    "You just played a wild card. Color? (Red 0, Yellow 1, Green 2, Blue 3)");
                            flag = true;
                            continue;
                        } else {
                            wild.setColor(5);
                        }
                        break;
                    }
                }
            }
            if (check()) {
                break;
            } else {
                this.turn = nextTurn();
                checkDeck(0);
            }
        }
    }

    /**
     * Check if player called uno or win
     * @return true if a player win, false is a player call uno
     * @throws IOException
     */
    public boolean check() throws IOException {
        if (players[turn].getNumOfCards() == 0) {
            sendToAll(turn, "win!");
            return true;
        } else if (players[turn].getNumOfCards() == 1) {
            sendToAll(turn, "called uno!");
        }
        return false;
    }

    /**
     *
     * @param numOfCards
     * @throws IOException
     */
    public void checkDeck(int numOfCards) throws IOException {
        if (deck.getNumOfCards() - numOfCards <= 0) {
            deck.setUpNewDeck(discard.reshuffle());
            for (int i = 0; i < clients.size(); ++i) {
                sendToPlayer(clients.get(i),
                        String.format("Deck is empty! Reshuffle discard pile.\tThere are %d cards in the deck now.", deck.getNumOfCards()));
            }
        }
    }

    /**
     * Send message to all players
     * @param idx current player
     * @param msg message to send
     * @throws IOException
     */
    private void sendToAll(int idx, String msg) throws IOException {
        for (int i = 0; i < clients.size(); ++i) {
            if (i != idx) {
                sendToPlayer(clients.get(i), "Player " + Integer.toString(idx) + " " + msg);
            } else {
                sendToPlayer(clients.get(i), "You " + msg);
            }
        }
    }

    /**
     * Send message to a player
     * @param client player need to receive message
     * @param msg message to send
     * @throws IOException
     */
    private void sendToPlayer(Socket client, String msg) throws IOException {
        PrintStream os = new PrintStream(client.getOutputStream());
        os.println(msg);
    }

    /**
     * Print the current game state. Information includes number of cards in deck, order of the game, card on the discard top.
     * It also prints the cards a player has and legal cards a player could play.
     */
    public String printCurrentGameState() {
        String strOrder = order == 1 ? "clockwise" : "counter-clockwise";
        String deckNum = Integer.toString(deck.getNumOfCards());
        String discardCard = wild.getColor() == 5 ? discard.getTop().toString() : wild.toString();
        String ret = String.format("You are Player No.%d and the order is %s.\tThere are %s cards in deck now.\t" +
                "Top card on the discard pile is (%s).\t%s", turn, strOrder, deckNum, discardCard, printPlayerCard(getTurn()));
        return ret;
    }

    /**
     * helper function for printCurrentGameState. It prints the cards a player has and legal cards a player could paly.
     * @param num index of current player
     */
    private String printPlayerCard(int num) {
        Player player = players[num];
        Card top = wild.getColor() == 5 ? discard.getTop() : wild;
        String playerCard = "";
        String ret = "";
        HashMap<Integer, ArrayList<Card>> hand = player.getHand();
        for (Map.Entry mapElement : hand.entrySet()) {
            for (Card card : (ArrayList<Card>)mapElement.getValue()) {
                playerCard += "[" + card.toString() + "] ";
            }
        }
        String legalStr = "";
        ArrayList<Card> legalCards = player.returnIfContainsLegal(top);
        for (Card card : legalCards) {
            legalStr += "<" + card.toString() + "> ";
        }
        if (legalCards.size() == 0) {
            Card drawCard = deck.drawTop();
            player.drawOneCard(drawCard);
            ret = String.format("You have %d cards in your hands: %s.\tYou don't have a card to play. You draw [%s]",
                    player.getNumOfCards(), playerCard, drawCard.toString());
        } else {
            ret = String.format("You have %d cards in your hands: %s.\tAnd you could play %s.\tOr do you want to draw a card?",
                    player.getNumOfCards(), playerCard, legalStr);
        }
        return ret;
    }

    /**
     * Get all the players
     * eturn array of players
     */
    public Player[] getPlayers() {
        return players;
    }

    /**
     * Getter for deck variable. Used for testing only.
     * @return
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Getter for discard variable. Used for testing only.
     * @return
     */
    public Discard getDiscard() {
        return discard;
    }

    /**
     * Getter for turn variable. Used for testing only.
     * @return
     */
    public int getTurn() {
        return turn;
    }

    /**
     * Initialization function for Game object.
     */
    public void init() {
        players = new Player[numOfPlayers];
        for (int i = 0; i < numOfPlayers; ++i) {
            players[i] = new Player();
        }
        deck = new Deck();
        discard = new Discard();
        wild = new NumberCard(-1, 5);
        turn = 0;
        order = 1;
    }

    /**
     * Return the index indicating the next player.
     * @return index of next player
     */
    public int nextTurn() {
        // Md operation that gives positive number only
        return (((turn + order) % numOfPlayers) + numOfPlayers) % numOfPlayers;
    }

    private boolean handleTurn(int idx) {
        boolean ret = false;
        Card top = wild.getColor() == 5 ? discard.getTop() : wild;
        Card card = handlePlayer(players[turn], top, idx);
        if (card.getType()) {
            switch (((ActionCard) card).getAction()) {
                case 0:
                    handleSkip();
                    break;
                case 1:
                    handleReverse();
                    break;
                case 2:
                    handleDrawTwo();
                    break;
                case 3:
                    ret = true;
                    break;
                case 4:
                    handleWildDrawFour();
                    ret = true;
                    break;
                case 5:
                    handleDoubleSkip();
                    break;
                case 6:
                    handleOneMoreRound();
                    break;
                default:
                    System.out.format("Player chose an illegal card");
            }
        }
        return ret;
    }

    /**
     * Handle the player's action. Will be refactored in the next assignment.
     * @param player player to be handled
     * @param card card on the discard pile
     * @param idx index of the card player will play if possible
     */
    public Card handlePlayer(Player player, Card card, int idx) {
        ArrayList<Card> cards = player.returnIfContainsLegal(card);
        if (cards.size() > 0) {
            Card playCard = cards.get(idx);
            player.playCard(playCard);
            discard.discardCard(playCard);
            return cards.get(idx);
        } else {
            player.drawOneCard(deck.drawTop());
            cards = player.returnIfContainsLegal(card);
            if (cards.size() > 0) {
                Card playCard = cards.get(idx);
                player.playCard(playCard);
                discard.discardCard(playCard);
                return cards.get(idx);
            }
        }
        return null;
    }

    /**
     * Handle skip card.
     */
    public void handleSkip() {
        turn = nextTurn();
    }

    /**
     * Handle reverse card.
     */
    public void handleReverse() {
        order = -order;
    }

    /**
     * Handle draw two card.
     */
    public void handleDrawTwo() {
        Player unlucky = players[nextTurn()];
        for (int i = 0; i < 2; ++i) {
            dealCardToPlayer(unlucky);
        }
    }

    /**
     * Handle wild card.
     * @param color color to be set for next turn
     */
    public void handleWild(int color) {
        wild.setColor(color);
    }

    /**
     * Handle wild draw four card.
     */
    public void handleWildDrawFour() {
        Player unlucky = players[nextTurn()];
        for (int i = 0; i < 4; ++i) {
            dealCardToPlayer(unlucky);
        }
        handleSkip();
    }

    /**
     * Handle double skip card.
     */
    public void handleDoubleSkip() {
        if (numOfPlayers <= 3) {
            handleOneMoreRound();
        } else {
            handleSkip();
            handleSkip();
        }
    }

    /**
     * Handle one more round card.
     */
    public void handleOneMoreRound() {
        for (int i = 0; i < numOfPlayers - 1; ++i) {
            turn = nextTurn();
        }
    }

    /**
     * Deal card on top of deck to one player.
     * @param player player to be dealt card to
     */
    private void dealCardToPlayer(Player player) {
        player.drawOneCard(deck.drawTop());
    }

    /**
     * Deal initial cards to all players.
     */
    private void dealInitialCardsToPlayers() {
        for (int i = 0; i < players.length; ++i) {
            Card[] temp = deck.drawMultipleTop(7);
            players[i].drawMultipleCards(temp);
        }
    }

    /**
     * Assign one card to discard. Re-assign if card is not number card.
     */
    private void assignCardToDiscard() {
        Card temp;
        do {
            temp = deck.drawTop();
            discard.discardCard(temp);
        } while(temp.getType());
    }
}
