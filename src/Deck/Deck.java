package Deck;

import Card.*;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private int numOfCards;
    private Card top;
    private ArrayList<Card> deck;
    private Random rand;

    /**
     * Constructor for Deck object.
     */
    public Deck() {
        numOfCards = 108;
        deck = new ArrayList<Card>();
        rand = new Random();
        initCards();
        assignTop();
    }

    /**
     * Constructor for Deck object. Used for testing only.
     * @param seed seed for random
     */
    public Deck(long seed) {
        numOfCards = 108;
        deck = new ArrayList<Card>();
        rand = new Random();
        rand.setSeed(seed);
        initCards();
        assignTop();
    }

    /**
     * Getter for number of cards.
     * @return number of cards in the deck
     */
    public int getNumOfCards() {
        return numOfCards;
    }

    /**
     * Initialization for all cards in the deck.
     */
    private void initCards() {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 10; ++j) {
                deck.add(new NumberCard(j, i));
            }
            for (int j = 1; j < 10; ++j) {
                deck.add(new NumberCard(j, i));
            }
            for (int j = 0; j < 3; ++j) {
                deck.add(new ActionCard(j, i));
                deck.add(new ActionCard(j, i));
            }
            deck.add(new ActionCard(3, 4));
            deck.add(new ActionCard(4, 4));
            deck.add(new ActionCard(5, i));
            deck.add(new ActionCard(6, i));
        }
    }

    /**
     * Draw cards from deck multiple times.
     * @param num number of cards to be drawn from deck
     * @return array of cards drawn from deck
     */
    public Card[] drawMultipleTop(int num) {
        Card[] ret = new Card[num];
        for (int i = 0; i < num; ++i) {
            ret[i] = drawTop();
        }
        return ret;
    }

    /**
     * Draw one card from deck.
     * @return card drawn from deck
     */
    public Card drawTop() {
        Card ret = top;
        deck.remove(top);
        numOfCards--;
        assignTop();
        return ret;
    }

    /**
     * Assign one card randomly to top.
     */
    private void assignTop() {
        if (numOfCards != 0) {
            int idx = rand.nextInt(numOfCards);
            top = deck.get(idx);
        }
    }

    /**
     * Set up a new deck when deck is empty.
     * @param cards Arraylist of cards to be set up as new deck
     */
    public void setUpNewDeck(ArrayList<Card> cards) {
        deck.addAll(cards);
        numOfCards = deck.size();
        assignTop();
    }
}
